﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLog;
using Torch;
using Torch.API;

namespace TestCommandPlugin {
    public class Main : TorchPluginBase {
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public override void Init(ITorchBase torch) {
            base.Init(torch);

            Log.Info("Starting TestCommandPlugin");
        }
    }
}
